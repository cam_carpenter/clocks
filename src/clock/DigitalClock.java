package clock;

import java.awt.Color;
import java.awt.Graphics;


/**
 * A view of a clock that looks like the digital display of typical alarm clock.
 * 
 * @author Chuck Cusack, 2001. Extensive refactoring, January 2013.
 */
public class DigitalClock extends ClockView {
	Digit	second1, second2, minute1, minute2, hour1, hour2, colon1, colon2;

	public DigitalClock(ClockInterface clock) {
		super(350, 150, clock);
		hour1 = new Digit(25, 25, 0);
		hour2 = new Digit(65, 25, 0);
		colon1 = new Digit(95, 25, 10);
		minute1 = new Digit(125, 25, 0);
		minute2 = new Digit(165, 25, 0);
		colon2 = new Digit(195, 25, 10);
		second1 = new Digit(225, 25, 0);
		second2 = new Digit(265, 25, 0);
	}
	
	/*
	 * Abstract methods from ClockView.
	 */

	@Override
	public void drawFace(Graphics g) {
		g.setColor(Color.black);
		g.fillRoundRect(5, 5, 325, 90, 50, 50);
		g.setColor(Color.red);
		g.fillRoundRect(15, 15, 305, 70, 40, 40);
		g.setColor(Color.black);
		g.fillRoundRect(20, 20, 295, 60, 30, 30);
		colon1.drawDigit(g);
		colon2.drawDigit(g);
	}

	@Override
	public void drawSeconds(Graphics g) {
		second1.setDigit(getSeconds() / 10);
		second2.setDigit(getSeconds() % 10);
		second1.drawDigit(g);
		second2.drawDigit(g);
	}

	@Override
	public void drawMinutes(Graphics g) {
		minute1.setDigit(getMinutes() / 10);
		minute2.setDigit(getMinutes() % 10);
		minute1.drawDigit(g);
		minute2.drawDigit(g);
	}

	@Override
	public void drawHours(Graphics g) {
		hour1.setDigit(getHours() / 10);
		hour2.setDigit(getHours() % 10);
		hour1.drawDigit(g);
		hour2.drawDigit(g);
	}
}
